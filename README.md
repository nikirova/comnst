
# comNST

Builds a DLL with all common declarations, classes and methods used by NST background applications


## Hardware requirements

** Any PC or laptop


## Operating system requirements

** Windows 10 ** 


## Nuget Package requirements

-	Microsoft.NETCore.UniversalWindowsPlatform ver 5.2.3
-	WindowsAzure.Storage ver 8.4.0

## Build the sample

1. Start Microsoft Visual Studio 2017 and select **File** \> **Open** \> **Project/Solution**.
2. Starting in the "Middleware" folder, go to the bkCloudManager subfolder. Double-click the Visual Studio 2017 Solution (.sln) file.
3. In solution configuration, select "Debug", "Any CPU".
4. Select **Deploy** \> **Deploy Solution**.


## Licensing

"The code in this project is licensed under NST license."
