﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace comNST
{
    public class NstMessage

    {
        // Resource Values
        public const string HUMMIDITY_AIR = "00";
        public const string HUMMIDITY_SOIL = "01";
        public const string TEMPERATURE_AIR = "02";
        public const string TEMPERATURE_SOIL = "03";
        public const string BATTERY_LEVEL = "04";
        public const string HUMMIDITY_AIR_NAME = "AirHumidity";
        public const string HUMMIDITY_SOIL_NAME = "SoilHumidity";
        public const string TEMPERATURE_AIR_NAME = "AirTemperature";
        public const string TEMPERATURE_SOIL_NAME = "SoilTemperature";
        public const string BATTERY_LEVEL_NAME = "Battery";


        // Commands
        public const string GET_COMMAND = "GET"; //Get value i.e get temperature value
        public const string THN_COMMAND = "THN"; // Threshold miN value
        public const string THX_COMMAND = "THX"; // Threshold maX value
        public const string SCH_COMMAND = "SCH"; // Schedule
        public const string RST_COMMAND = "RST"; // Reset CMD
        public const string RSG_COMMAND = "RSG"; // Reset CMG
        public const string PNG_COMMAND = "PNG"; // Ping
        public const string HBE_COMMAND = "HBE"; // Heart Beat (ping to CMG)
        public const string SUB_COMMAND = "SUB"; // Subscribe
        public const string UNS_COMMAND = "UNS"; // Unsubscribe


        //time
        public const int WAITING_TIME = 300; // 5 minutes in seconds

        // Response Codes
        public const string OK_RESPONSE = "00"; // Valid for a Response to a Command
        public const string NOK_RESPONSE = "01"; // Not Valid for a Response to a Command
        public const string BUSY_RESPONSE = "02"; // Receiver is busy
        public const string RNF_RESPONSE = "03"; // Resource is not found (error 404)
        public const string SNF_RESPONSE = "04"; // Server not found (CMD is not in the network)
        public const string WVDR_RESPONSE = "05"; // Wrong Data Received.
        public const string TELE_RESPONSE = "99"; // Telemetry Data
        public const string ALERT_RESPONSE = "98"; // Alert: Currently "Wrong Data" at SUB command 
        public const string RST_CMG = "11"; // When CMG resets
        public const string RST_CMD = "12"; // When CMD resets
        public const string HBE_ALERT_RESPONSE = "97"; // Alert - CMG is DEAD


        private const string commandKey = "command";
        private const string ipnodeKey = "ipnode";
        private const string valueKey = "value";
        private const string timestampKey = "timeCreated";
        private const string resourceKey = "resource";
        private const string appIdKey = "appid";
        private const string tokenKey = "token";
        private const string tokenlengthKey = "tokenlength";
        private const string responseKey = "response";
        private const string azuremessageidKey = "azureMessageId";

        private string command;
        private string ipnode;
        private double mvalue;
        private DateTime timestamp;
        private string resource;
        private string appid;
        private string token;
        private string tokenlength;
        private string response;
        private string azuremessageid;

        public NstMessage()
        {
            command = "";
            ipnode = "";
            mvalue = 0.0;
            timestamp = DateTime.Now.ToLocalTime();
            resource = "";
            appid = "";
            token = "";
            tokenlength = "08";
            response = "";
            azuremessageid = "";
        }

        public NstMessage(string jsonString) : this()
        {
            JsonObject jsonObject = JsonObject.Parse(jsonString);
            command = jsonObject.GetNamedString(commandKey, "");
            resource = jsonObject.GetNamedString(resourceKey, "");
            ipnode = jsonObject.GetNamedString(ipnodeKey, "");
            mvalue = jsonObject.GetNamedNumber(valueKey, 0.0);


            appid = jsonObject.GetNamedString(appIdKey, "");
            token = jsonObject.GetNamedString(tokenKey, "");
            tokenlength = jsonObject.GetNamedString(tokenlengthKey, "");
            response = jsonObject.GetNamedString(responseKey, "");
            azuremessageid = jsonObject.GetNamedString(azuremessageidKey, "");

            string timecreated = jsonObject.GetNamedString(timestampKey, "");
            timestamp = Convert.ToDateTime(timecreated);

        }

        public string Stringify()
        {


            JsonObject jsonObject = new JsonObject();
            jsonObject[commandKey] = JsonValue.CreateStringValue(command);
            jsonObject[resourceKey] = JsonValue.CreateStringValue(resource);
            jsonObject[ipnodeKey] = JsonValue.CreateStringValue(ipnode);
            jsonObject[valueKey] = JsonValue.CreateNumberValue(mvalue);
            jsonObject[timestampKey] = JsonValue.CreateStringValue(Timestamp);
            jsonObject[appIdKey] = JsonValue.CreateStringValue(appid);
            jsonObject[tokenKey] = JsonValue.CreateStringValue(token);
            jsonObject[tokenlengthKey] = JsonValue.CreateStringValue(tokenlength);
            jsonObject[responseKey] = JsonValue.CreateStringValue(response);
            jsonObject[azuremessageidKey] = JsonValue.CreateStringValue(azuremessageid);


            return jsonObject.Stringify();
        }

        public string Response
        {
            get
            {
                return response;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                response = value;
            }
        }
        public string Command
        {
            get
            {
                return command;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                command = value;
            }
        }
        public string Appid
        {
            get
            {
                return appid;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                appid = value;
            }
        }
        public string Token
        {
            get
            {
                return token;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                token = value;
            }
        }

        public string Tokenlength
        {
            get
            {
                return tokenlength;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                tokenlength = value;
            }
        }

        public string Resource
        {
            get
            {
                return resource;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                resource = value;
            }
        }

        public string Ipnode
        {
            get
            {
                return ipnode;
            }
            set
            {
                ipnode = value;
            }
        }

        public string Azuremessageid
        {
            get
            {
                return azuremessageid;
            }
            set
            {
                azuremessageid = value;
            }
        }


        public double Value
        {
            get
            {
                return mvalue;
            }
            set
            {
                mvalue = value;
            }
        }



        public string Timestamp
        {
            get
            {

                return timestamp.ToString("yyyy-MM-dd h:mm:ss tt");
            }
            set
            {
                timestamp = Convert.ToDateTime(value);
            }
        }


    }

}
