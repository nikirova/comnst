﻿using System.Threading.Tasks;
using System;
using System.Diagnostics;
using System.IO;
using Windows.Storage;
using Windows.ApplicationModel;

namespace comNST
{
    public class Logging
    {
        public static async Task WriteSystemLog(string strFormat, params string[] strParams)
        {
            await WriteLog("System", strFormat, strParams); 
        }

        public static async Task WriteDebugLog(string strFormat, params string[] strParams)
        {
            await WriteLog("Debug", strFormat, strParams);
        }

        private static async Task WriteLog(string strLog, string strFormat, params string[] strParams)
        {
            DateTime dtNow;
            string strLogFile;
            string strLogData;
            StorageFolder storageFolder;
            StorageFile storageFile;

            try
            {
                dtNow = DateTime.Now;
                strLogFile = string.Format("{0}_{1}_{2}.Log", Package.Current.DisplayName.ToString(), strLog, dtNow.ToString("yyyyMMdd"));
                strLogData = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.ff ") + string.Format(strFormat, strParams);

                Debug.WriteLine(strLogData);

                // Apply Asynchronous Lock here
                {
                    storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                    storageFile = await storageFolder.CreateFileAsync(strLogFile, CreationCollisionOption.OpenIfExists);

                    using (StreamWriter writer = new StreamWriter(await storageFile.OpenStreamForWriteAsync()))
                    {
                        writer.BaseStream.Seek(0, SeekOrigin.End);
                        await writer.WriteLineAsync(strLogData);
                        writer.Flush();
                    }
                }
            }
            catch (Exception eException)
            {
                Debug.WriteLine(string.Format("Error: WriteSystemLog() {0}", eException.Message));
            }
        }
    }
}




