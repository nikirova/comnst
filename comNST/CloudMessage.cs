﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace comNST
{
    public class CloudMessage
    {

        public class AlertData
        {

            string deviceId;
            string gatewayId;
            string message;
            string objectType;
            public AlertData()
            {
                DeviceId = "";
                GatewayId = "";
                Message = "";
                ObjectType = "AlertInfo";
            }
            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["DeviceId"] = JsonValue.CreateStringValue(DeviceId);
                jsonObject["GatewayId"] = JsonValue.CreateStringValue(GatewayId);
                jsonObject["Message"] = JsonValue.CreateStringValue(Message);
                jsonObject["ObjectType"] = JsonValue.CreateStringValue(ObjectType);
                return jsonObject.Stringify();
            }

            public string DeviceId
            {
                get
                {
                    return deviceId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    deviceId = value;
                }
            }

            public string GatewayId
            {
                get
                {
                    return gatewayId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    gatewayId = value;
                }
            }

            public string Message
            {
                get
                {
                    return message;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    message = value;
                }
            }

            public string ObjectType
            {
                get
                {
                    return objectType;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    objectType = value;
                }
            }

        }

        public class RaiseAlertData
        {

            string deviceId;
            double alert;
            public RaiseAlertData()
            {
                DeviceId = "";
                Alert = 1;
            }
            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["DeviceId"] = JsonValue.CreateStringValue(DeviceId);
                jsonObject["Alert"] = JsonValue.CreateNumberValue(Alert);
                return jsonObject.Stringify();
            }

            public string DeviceId
            {
                get
                {
                    return deviceId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    deviceId = value;
                }
            }

            public double Alert
            {
                get
                {
                    return alert;
                }
                set
                {
                    alert = value;
                }
            }

        }

        public class ResponseCommandData
        {

            string gatewayName;
            string messageId;
            public ResponseCommandData()
            {
                GatewayName = "";
                MessageId = "";
            }
            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["cgateway"] = JsonValue.CreateStringValue(GatewayName);
                jsonObject["cmessageid"] = JsonValue.CreateStringValue(MessageId);
                return jsonObject.Stringify();
            }

            public string GatewayName
            {
                get
                {
                    return gatewayName;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    gatewayName = value;
                }
            }
            public string MessageId
            {
                get
                {
                    return messageId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    messageId = value;
                }
            }


        }



        // Soil Humidity
        public class SoilHumidityData
        {

            string deviceId;
            double soilHumidity;
            public SoilHumidityData()
            {
                DeviceId = "";
                SoilHumidity = 0.0;
            }
            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["DeviceId"] = JsonValue.CreateStringValue(DeviceId);
                jsonObject["SoilHumidity"] = JsonValue.CreateNumberValue(SoilHumidity);
                return jsonObject.Stringify();
            }

            public string DeviceId
            {
                get
                {
                    return deviceId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    deviceId = value;
                }
            }

            public double SoilHumidity
            {
                get
                {
                    return soilHumidity;
                }
                set
                {
                    soilHumidity = value;
                }
            }

        }


        public class AirHumidityData
        {

            string deviceId;
            double airHumidity;
            public AirHumidityData()
            {
                DeviceId = "";
                AirHumidity = 0.0;
            }
            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["DeviceId"] = JsonValue.CreateStringValue(DeviceId);
                jsonObject["AirHumidity"] = JsonValue.CreateNumberValue(AirHumidity);
                return jsonObject.Stringify();
            }

            public string DeviceId
            {
                get
                {
                    return deviceId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    deviceId = value;
                }
            }

            public double AirHumidity
            {
                get
                {
                    return airHumidity;
                }
                set
                {
                    airHumidity = value;
                }
            }

        }

        public class AirTemperatureData
        {

            string deviceId;
            double airTemperature;
            public AirTemperatureData()
            {
                DeviceId = "";
                AirTemperature = 0.0;
            }

            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["DeviceId"] = JsonValue.CreateStringValue(DeviceId);
                jsonObject["AirTemperature"] = JsonValue.CreateNumberValue(AirTemperature);
                return jsonObject.Stringify();
            }
            public string DeviceId
            {
                get
                {
                    return deviceId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    deviceId = value;
                }
            }

            public double AirTemperature
            {
                get
                {
                    return airTemperature;
                }
                set
                {
                    airTemperature = value;
                }
            }

        }

        public class SoilTemperatureData
        {

            string deviceId;
            double soilTemperature;
            public SoilTemperatureData()
            {
                DeviceId = "";
                SoilTemperature = 0.0;
            }

            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["DeviceId"] = JsonValue.CreateStringValue(DeviceId);
                jsonObject["SoilTemperature"] = JsonValue.CreateNumberValue(SoilTemperature);
                return jsonObject.Stringify();
            }

            public string DeviceId
            {
                get
                {
                    return deviceId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    deviceId = value;
                }
            }

            public double SoilTemperature
            {
                get
                {
                    return soilTemperature;
                }
                set
                {
                    soilTemperature = value;
                }
            }

        }
        public class BatteryData
        {

            string deviceId;
            double battery;
            public BatteryData()
            {
                DeviceId = "";
                Battery = 0.0;
            }
            public string Stringify()
            {
                JsonObject jsonObject = new JsonObject();
                jsonObject["DeviceId"] = JsonValue.CreateStringValue(DeviceId);
                jsonObject["Battery"] = JsonValue.CreateNumberValue(Battery);
                return jsonObject.Stringify();
            }

            public string DeviceId
            {
                get
                {
                    return deviceId;
                }
                set
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("value");
                    }
                    deviceId = value;
                }
            }

            public double Battery
            {
                get
                {
                    return battery;
                }
                set
                {
                    battery = value;
                }
            }

        }



    }
}
