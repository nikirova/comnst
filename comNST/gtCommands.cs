﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System;
using Windows.System.Threading;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Windows.Storage;

namespace comNST
{
    public class gtCommands
    {

        public static void restartDragonBoard(ThreadPoolTimer timer)
        {

            DateTime timestamp = DateTime.Now.ToLocalTime();
            Logging.WriteDebugLog("Restart DragonBoard at: {0}", timestamp.ToString());

            // BE CAREFULL!!!!!
            // TUTORIAL Used: https://marcominerva.wordpress.com/2016/12/13/how-to-shutdown-a-windows-10-iot-core-device-from-a-uwp-app/

            // BE CAREFULL
            // NIKI
            // ADD Reference to extension: Windows IoT Extensions for the UWP 10.0.15063.0
            // MARIA
            // ADD Reference to extension: Windows IoT Extensions for the UWP 10.0.14393.0

            // Restarts the device within 5 seconds:

            ShutdownManager.BeginShutdown(ShutdownKind.Restart, TimeSpan.FromSeconds(1));
        }

        public static async Task<bool> GetDBFileFromAzure()
        {
            try
            {

                await Logging.WriteDebugLog("Start downloading process .........................");

                CloudStorageAccount storageAccount = new CloudStorageAccount(
                       new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(
                       "nydorstoragetest",
                       "0ppe15mnVCdFPfS81Zxj88jRIgAz6hCRorsaKgVEA7jyqd7NMfGr4sjhW0iWkIfpBSb38BWHZB8s4LFS/RRD+Q=="), true);

                // Create a blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Get a reference to the container named “gatewaydbcontainer”
                CloudBlobContainer container = blobClient.GetContainerReference("gatewaydbcontainer");

                // Get a reference to a blob named "gateway.db".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference("gateway.db");

                //  create a local file

                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("Gateway.db", CreationCollisionOption.ReplaceExisting);

                //  download from Azure Storage
                await blockBlob.DownloadToFileAsync(file);

                // copy database file to c:\\Database folder in order to be accessible by all applications
                string destinationFileName = "c:\\Database\\Gateway.db";
                string sourceFileName = "c:\\Data\\Users\\DefaultAccount\\AppData\\Local\\Packages\\BKCloudManager-uwp_dryy15wfsn6zj\\LocalState\\Gateway.db";
                System.IO.File.Copy(sourceFileName, destinationFileName, true);
                return true;

            }
            catch (Exception ex)
            {
                //Debug.WriteLine("GetDBFileFromAzure: Exception thrown: " + ex.Message);
                string exMessage = "GetDBFileFromAzure: Exception thrown - Message: " + ex.Message + "\n";
                exMessage += "-- StackTrace: " + ex.StackTrace + "\n";
                exMessage += "-- GetBaseException(): " + ex.GetBaseException();
                await Logging.WriteSystemLog("GetDBFileFromAzure - {0}", exMessage);
                return false;
            }


        }
    }
}
